<?php
/*
Template Name: Archive
 */

get_header(); ?>

<section class="main-wrapper archives">

<section class="main" role="main">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php
	if ( have_posts() )
		the_post();
?>

	<header class="post-header">
	
		<h1 class="post-title"><?php the_title(); ?></h1>
		
	</header>

	<section class="post-content">
		
		<h2>Archives by Month:</h2>
		<ul>
			<?php wp_get_archives('type=monthly'); ?>
		</ul>
		
		<h2>Archives by Subject:</h2>
		<ul>
			 <?php wp_list_categories('title_li='); ?>
		</ul>
		
	</section>	

</article>
	
</section><!-- .main -->

<?php get_sidebar(); ?>

</section><!-- .mainwrapper -->

<?php get_footer(); ?>