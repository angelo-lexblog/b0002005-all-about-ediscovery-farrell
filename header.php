<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title(''); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<!--[if lt IE 9]>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/modernizr-1.7.min.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/selectivizr-min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'stylesheet_directory' ); ?>/ie7.css" />
<![endif]-->
<!--Google Apps verification -->
<meta name="google-site-verification" content="2U2wXXZQF218OLuomiZF9OnOzBqTm_lJ6m_WY4ARDlA" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js"></script>
<script src="<?php bloginfo( 'stylesheet_directory' ); ?>/js/jQuery.evtpaginate.js"></script>
<link rel="stylesheet" type="text/css" media="print" href="<?php bloginfo( 'stylesheet_directory' ); ?>/print.css" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
 
<?php
    /* We add some JavaScript to pages with the comment form
     * to support sites with threaded comments (when in use).
     */
    if ( is_singular() && get_option( 'thread_comments' ) )
        wp_enqueue_script( 'comment-reply' );
 
    /* Always have wp_head() just before the closing </head>
     * tag of your theme, or you will break many plugins, which
     * generally use this hook to add elements to <head> such
     * as styles, scripts, and meta tags.
     */
    wp_head();
?>
</head>
 
<body <?php body_class(); ?>>

<img class="print-header" src="<?php bloginfo( 'stylesheet_directory' ); ?>/images/print-header.jpg" alt="Header graphic for print" />

<div class="blog-wrapper">
 
    <header class="blog-header">
        <hgroup role="banner">
            <?php $heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'span'; ?>
			<<?php echo $heading_tag; ?> class="blog-title">
				<a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
			</<?php echo $heading_tag; ?>>
			<?php if ( get_bloginfo( 'description' ) ) : ?>
           	<?php $description_tag = ( is_home() || is_front_page() ) ? 'h2' : 'span'; ?>
				<<?php echo $description_tag; ?> class="blog-description">
					<?php bloginfo( 'description' ); ?>
				</<?php echo $description_tag; ?>>
			<?php endif; ?>
        </hgroup>
 
        <?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to the 'starkers_menu' function which can be found in functions.php.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
        <?php wp_nav_menu( array( 'container' => 'nav', 'fallback_cb' => 'starkers_menu', 'theme_location' => 'primary' ) ); ?>

		<?php get_sidebar ('header'); // call header widget area ?>
		
    </header>