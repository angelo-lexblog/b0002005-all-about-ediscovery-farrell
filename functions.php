<?php

/**
 * Register widgetized areas.
 *
 * @since lxb Theme 1.0
 */
function lxb_widgets() {
	// Header Widget Area
	register_sidebar( array(
		'name' => __( 'Header Widget Area' ),
		'id' => 'header-widgets',
		'description' => __( 'Header widget area, typically for bylines and search' ),
		'before_widget' => '<section class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Sidebar Widget Area, loaded after the main page content
	register_sidebar( array(
		'name' => __( 'Main Sidebar' ),
		'id' => 'sidebar',
		'description' => __( 'Sidebar widget area, for most widgets'),
		'before_widget' => '<section class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// First Footer Widget Area, typically for address and social link icons
	register_sidebar( array(
		'name' => __( 'Footer Contact Widgets Area' ),
		'id' => 'footer-contact',
		'description' => __( 'The first footer widget area, typically for address and social link icons' ),
		'before_widget' => '<section class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Second Footer Widget Area, typically for links and text widgets
	register_sidebar( array(
		'name' => __( 'Footer Extras Widgets Area' ),
		'id' => 'footer-extras',
		'description' => __( 'The second footer widget area, typically for links and text widgets' ),
		'before_widget' => '<section class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	
	// Contact Page Sidebar
	register_sidebar( array(
		'name' => __( 'Contact Page Sidebar' ),
		'id' => 'contact-sidebar',
		'description' => __( 'Widget area for contact page sidebar' ),
		'before_widget' => '<section class="widget %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
/** Register sidebars by running lxb_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'lxb_widgets' );

/**
 * Add lxb-specific scripts
 *
 * @since lxb Theme 1.0
 */
function add_lxb_scripts() {
  wp_enqueue_script('evtpaginate', get_bloginfo( 'stylesheet_directory' ).'/js/jQuery.evtpaginate.js', array('jquery'), false, true);
  wp_enqueue_script('lxb_default', get_bloginfo( 'stylesheet_directory' ).'/js/lxb_default.js', array('jquery','jgfeed','evtpaginate'), false, true);
}

add_action('after_setup_theme','add_lxb_scripts');

/**
 * Assign "read more" link text for excerpts
 *
 * @since lxb Theme 1.0
 */
 
 
function new_excerpt_more($more) {
	return false; //'... hello <a class="more" href="' . get_permalink() . '">Continue Reading</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function lxb_nyh_add_feed_continue_reading_link($content){
	if(is_feed()){
		$content = $content.'&hellip; <a class="more" href="' . get_permalink() . '">Continue Reading</a>';
	}
	return $content;
}
add_filter('get_the_excerpt', 'lxb_nyh_add_feed_continue_reading_link');




/**
 * Set the number of posts to display in different types of templates
 *
 * @since lxb Theme 1.0
 */
function custom_posts_per_page($query){
    //if(is_home()){ /* let defaults be set in settings for the homepage */
    //$query->set('posts_per_page',8);
    //}
    //if(is_search()){ /* let defaults be set in settings for the searh results page */
    //$query->set('posts_per_page',8);
    //}
    if(is_archive()){
        $query->set('posts_per_page',24); // show 24
}
}
 
    add_action('pre_get_posts','custom_posts_per_page');

/**
 * Include only posts in search results
 *
 * @since lxb Theme 1.0
 */
function include_in_search($query) {
	if ($query->is_search) {
	$query->set('post_type', 'post');
	}
	return $query;
	}

	add_filter('pre_get_posts','include_in_search');

/**
 * Co-authors in RSS and other feeds
 * /wp-includes/feed-rss2.php uses the_author(), so we selectively filter the_author value
 */
function db_coauthors_in_rss( $the_author ) {

if ( !is_feed() || !function_exists( 'coauthors' ) )
   return $the_author;

return coauthors( null, null, null, null, false );
}
add_filter( 'the_author', 'db_coauthors_in_rss' );

?>
