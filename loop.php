<?php
/**
 * The loop that displays posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

<?php if ( function_exists('yoast_breadcrumb') && is_single() ) { // display breadcrumbs on single posts only
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>

<?php /* If there are no posts to display, such as an empty archive page */ ?>
<?php if ( ! have_posts() ) : ?>
        <h1 class="post-title"><?php _e( 'Not Found', 'starkers' ); ?></h1>
            <p><?php _e( 'Apologies, but no results were found for the request. Perhaps searching will help find a related post.', 'starkers' ); ?></p>
            <?php get_search_form(); ?>
<?php endif; ?>
 
<?php $i=0; // start counter for archive grid ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php if ( is_archive() && ! is_author() ) : // if this is an archive page ?>
 	<?php if($i%3==0) { // if counter is at a multiple of 3, start a row ?>
		<section class="grid-row">
		<?php } ?>
<?php endif; ?>
 
<?php /* How to display posts of the Gallery format. The gallery category is the old way. */ ?>
 
    <?php if ( ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) ) || in_category( _x( 'gallery', 'gallery category slug', 'starkers' ) ) ) : ?>
     
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="post-header">
                <?php $title_tag = ( is_single() || is_page() ) ? 'h1' : 'h2'; // make single post and page titles h1 and all other post titles h2 ?>
               	<<?php echo $title_tag; ?> class="post-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'starkers' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</<?php echo $title_tag; ?>>
 
            	<span class="post-byline">By
               <?php if ( function_exists('coauthors') ) {
						$co = new CoAuthorsIterator();
						$co->iterate();
						the_author_link();
						while($co->iterate()){
						    print $co->is_last() ? ' and ' : ', ';
						    the_author_link();
						}
					} else {
					the_author_link();
					}
				?>
               on <time class="post-date"><?php the_time('F j, Y') ?></time></span>
            </header>
 
			<section class="post-content">
<?php if ( post_password_required() ) : ?>
                <?php the_content(); ?>
<?php else : ?>
<?php $images = get_children( array( 'post_parent' => $post->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order', 'order' => 'ASC', 'numberposts' => 999 ) );
    if ( $images ) :
        $total_images = count( $images );
        $image = array_shift( $images );
        $image_img_tag = wp_get_attachment_image( $image->ID, 'thumbnail' ); ?>
        
        <a href="<?php the_permalink(); ?>"><?php echo $image_img_tag; ?></a>
         
        <p><?php printf( _n( 'This gallery contains <a %1$s>%2$s photo</a>.', 'This gallery contains <a %1$s>%2$s photos</a>.', $total_images, 'starkers' ), 'href="' . get_permalink() . '" title="' . sprintf( esc_attr__( 'Permalink to %s', 'starkers' ), the_title_attribute( 'echo=0' ) ) . '" rel="bookmark"', number_format_i18n( $total_images )); ?></p>

	<?php endif; ?>
     
    <?php the_excerpt(); ?>
 
<?php endif; ?>
		</section>
            <footer class="post-footer">
	            <?php if ( function_exists( 'get_post_format' ) && 'gallery' == get_post_format( $post->ID ) ) : ?>
	            <a href="<?php echo get_post_format_link( 'gallery' ); ?>" title="<?php esc_attr_e( 'View Galleries', 'starkers' ); ?>"><?php _e( 'More Galleries', 'starkers' ); ?></a> | 
	            
	            <?php elseif ( in_category( _x( 'gallery', 'gallery category slug', 'starkers' ) ) ) : ?>
	            <a href="<?php echo get_term_link( _x( 'gallery', 'gallery category slug', 'starkers' ), 'category' ); ?>" title="<?php esc_attr_e( 'View posts in the Gallery category', 'starkers' ); ?>"><?php _e( 'More Galleries', 'twentyten' ); ?></a> | 
	            
	            <?php endif; ?>
	
	            <span class="post-comments"><?php comments_popup_link( __( 'Comment', 'starkers' ), __( '1 Comment', 'starkers' ), __( '% Comments', 'starkers' ) ); ?></span>

				<?php
	                $tags_list = get_the_tag_list( '', ', ' );
	                if ( $tags_list ):
	                    the_tags('<span class="post-tags">Tags: ', ', ', '</span>');
	            endif; ?>
            </footer>
        </article>
 
<?php /* How to display posts of the Aside format. The asides category is the old way. */ ?>
    
    <?php elseif ( ( function_exists( 'get_post_format' ) && 'aside' == get_post_format( $post->ID ) ) || in_category( _x( 'asides', 'asides category slug', 'starkers' ) )  ) : ?>
     
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
		<section class="post-content">
        <?php if ( is_archive() || is_search() ) : // Display excerpts for archives and search. ?>
                <?php the_excerpt(); ?>
        <?php else : ?>
                <?php the_content( __( 'Continue Reading', 'starkers' ) ); ?>
        <?php endif; ?>
         </section>

            <footer class="post-footer">
                <span class="post-comments"><?php comments_popup_link( __( 'Comment', 'starkers' ), __( '1 Comment', 'starkers' ), __( '% Comments', 'starkers' ) ); ?></span>

				<?php
	                $tags_list = get_the_tag_list( '', ', ' );
	                if ( $tags_list ):
	                    the_tags('<span class="post-tags">Tags: ', ', ', '</span>');
	            endif; ?> 
            </footer>
        </article>
 
<?php /* How to display all other posts. */ ?>
 
    <?php else : ?>
     
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         
			<header class="post-header">
				<?php $title_tag = ( is_single() || is_page() ) ? 'h1' : 'h2'; // make single post and page titles h1 and all other post titles h2 ?>
               	<<?php echo $title_tag; ?> class="post-title">
					<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'starkers' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
				</<?php echo $title_tag; ?>>
 
                <span class="post-byline">By
               <?php if ( function_exists('coauthors') ) {
						$co = new CoAuthorsIterator();
						$co->iterate();
						the_author_link();
						while($co->iterate()){
						    print $co->is_last() ? ' and ' : ', ';
						    the_author_link();
						}
					} else {
					the_author_link();
					}
				?>
               on <time class="post-date"><?php the_time('F j, Y') ?></time></span>
				
				<?php
	                $category_list = get_categories( '', ', ' );
	                if ( $category_list ):
						print '<span class="post-categories">Posted in ';
						the_category(', ');
						print '</span>';
	            endif; ?>
            </header>
 
			<section class="post-content">
    			<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
                	<?php echo get_the_excerpt().'&hellip; <a class="more" href="' . get_permalink() . '">Continue Reading</a>'; ?>
    			<?php else : ?>
                	<?php the_content( __( 'Continue Reading', 'starkers' ) ); ?>
    			<?php endif; ?>
			</section>
   
            <?php if ( is_home() || is_single() ) : // Only display social buttons on the homepage and on individual posts. ?>
	            <footer class="post-footer">
					<?php if(function_exists('lxb_nascar')){ echo lxb_nascar(); } ?>				
					<div class="post-tools">
						<?php if ( is_single() ) : // on single posts, display the print link ?>
							<span class="post-print"><a href="javascript:window.print()">Print</a></span>
						<?php endif; ?>
					
		                <span class="post-comments"><?php comments_popup_link( __( 'Comment', 'starkers' ), __( '1 Comment', 'starkers' ), __( '% Comments', 'starkers' ) ); ?></span>
					</div>
	
					<?php
		                $tags_list = get_the_tag_list( '', ', ' );
		                if ( $tags_list ):
		                    the_tags('<span class="post-tags">Tags: ', ', ', '</span>');
		            endif; ?> 
	            </footer>
	    	<?php endif ?>
		</article>
 
    <?php endif; // This was the if statement that broke the loop into three parts based on categories. ?>

<?php if ( is_archive() && ! is_author() ) : // if this is an archive page ?>
	<?php $i++;
			if($i%3==0) { // if counter is at a multiple of 3, close the wrapper ?>
			</section>
			<?php } ?>
<?php endif; ?>
 
<?php endwhile; // End the loop. Whew. ?>

<?php if ( is_archive() && ! is_author() ) : // if this is an archive page ?>
	<?php
		if($i%3!=0) { // put closing wrapper here if loop is not exactly at a multiple of 3 ?>
		</section>
		<?php } ?>
<?php endif; ?>
 
<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
    <nav class="pagination">
		<ul>
	        <li class="pagination-new"><?php previous_posts_link( __( 'Newer Posts', 'starkers' ) ); // newer posts ?></li>
	        <li class="pagination-old"><?php next_posts_link( __( 'Older Posts', 'starkers' ) ); // older posts ?></li>
		</ul>
    </nav>
<?php endif; ?>