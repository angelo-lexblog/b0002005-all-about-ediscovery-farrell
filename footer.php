<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers HTML5 3.0
 */
?>

	<footer class="blog-footer" role="complementary">
	
		<div class="blog-footer-inner-wrapper">
		
		<section class="colophon">
			<span class="copyright-info">Copyright &copy; <?php print date(Y); ?>, <?php echo get_option('lxb_firmname'); ?>. All Rights Reserved.</span>
			
			<a href="http://lexblog.com" class="lexblog">Strategy, design, marketing &amp; support by LexBlog</a>
		</section>	
		
			<div class="contact-wrapper">
				<a class="blog-title" href="<?php echo home_url( '/' ) ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
		
				<?php if ( is_active_sidebar( 'footer-contact' ) ) : ?>
						<section class="footer-contact" role="complementary">
							<?php dynamic_sidebar( 'footer-contact' ); ?>
						</section>
				<?php endif; ?>
			</div><!-- contact-wrapper -->
		
			<?php if ( is_active_sidebar( 'footer-extras' ) ) : ?>
					<section class="footer-extras" role="complementary">
						<?php dynamic_sidebar( 'footer-extras' ); ?>
					</section>
			<?php endif; ?>
		
			
		
		<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => '', 'container' => 'nav', 'container_class' => 'legal', 'container_id' => '', 'items_wrap' => '<ul>%3$s</ul>', ) ); ?>

		</div><!-- .blog-footer-inner-wrapper -->

	</footer>
	
</div><!-- close wrapper div started in header.php -->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>

	<script type="text/javascript">
		var addthis_config = addthis_config||{};
		addthis_config.pubid = 'ra-53ecfb2704b31343';
	</script>
	<script src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53ecfb2704b31343"></script>
</body>
</html>