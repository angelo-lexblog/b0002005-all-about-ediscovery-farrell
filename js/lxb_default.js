jQuery(document).ready( function($) {
  function cleartext(field) {
  if (field.defaultValue == field.value) field.value="";
  }
  function resetfield(field, newvalue) {
  if (field.value == "") field.value=newvalue;
  }

  $.jGFeed('http://blogs.lexblognetwork.com/bloglist/index.xml',
  function(feeds){
    // Check for errors
    if(!feeds){
      // there was an error
      return false;
    }
    // do whatever you want with feeds here
    for(var i=0; i<feeds.entries.length; i++){
      var entry = feeds.entries[i];
      $('#network-list').append('<li><a href="'+entry.link+'">'+entry.title+'</a></li>');
    }
  }, 1000);

  function paginate() {
    $(function(){
      var list = $('#network-list');
      $('.prev').click(function(){
        list.trigger('prev.evtpaginate');
        return false;
      });
      $('.next').click(function(){
        list.trigger('next.evtpaginate');
        return false;
      });
      list.bind( 'finished.evtpaginate', function(e, num, isFirst, isLast){
                          if(isLast) {
                              $('.next').addClass('end'); 
                          }
                          else {
                              $('.next').removeClass('end');
                          }
                      });
      list.bind( 'finished.evtpaginate', function(e, num, isFirst, isLast){
                          if(isFirst) {
                              $('.prev').addClass('end'); 
                          }
                          else {
                              $('.prev').removeClass('end');
                          }
                      });
      list.evtpaginate({perPage:10});
    });
  }

  setTimeout(function() { paginate(); },1000);

  var addthis_config = {
            username: "lexblog",
            services_compact: 'email, buzz, delicious, digg, facebook, favorites, friendfeed, linkedin, stumbleupon, twitter'
  }
});
